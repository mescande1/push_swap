/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_b_operations.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/01 16:24:02 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 10:06:59 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	sb(t_flap **topa, t_flap **topb)
{
	(void)topa;
	(*topb)->val += (*topb)->next->val;
	(*topb)->next->val = (*topb)->val - (*topb)->next->val;
	(*topb)->val -= (*topb)->next->val;
	if (write(STDOUT_FILENO, "sb\n", 3))
		return ;
}

void	pb(t_flap **topa, t_flap **topb)
{
	t_flap	*newtopa;
	t_flap	*newtopb;

	if ((!*topa))
		return ;
	newtopb = *topa;
	newtopa = (*topa)->next;
	(*topa)->prev->next = (*topa)->next;
	(*topa)->next->prev = (*topa)->prev;
	if (*topa != newtopa)
		*topa = newtopa;
	else
		*topa = NULL;
	if (!(*topb))
	{
		*topb = newtopb;
		(*topb)->prev = *topb;
	}
	newtopb->next = *topb;
	newtopb->prev = (*topb)->prev;
	(*topb)->prev = newtopb;
	newtopb->prev->next = newtopb;
	*topb = newtopb;
	if (write(STDOUT_FILENO, "pb\n", 3))
		return ;
}

void	rb(t_flap **topa, t_flap **topb)
{
	(void)topa;
	*topb = (*topb)->next;
	if (write(STDOUT_FILENO, "rb\n", 3))
		return ;
}

void	rrb(t_flap **topa, t_flap **topb)
{
	(void)topa;
	*topb = (*topb)->prev;
	if (write(STDOUT_FILENO, "rrb\n", 4))
		return ;
}
