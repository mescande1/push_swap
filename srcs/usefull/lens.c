/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lens.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 15:56:16 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 10:04:04 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	spit_len(t_flap *top)
{
	const t_flap	*save = top;
	int				len;

	len = 0;
	while (top && (top != save || !len))
	{
		top = top->next;
		len++;
	}
	return (len);
}
