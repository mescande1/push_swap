/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_placed_spitflap.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 19:08:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/26 12:33:04 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	is_val_placed_ascending_order(int val, t_flap **top)
{
	return ((((*top)->val > val && val > (*top)->prev->val)
			|| ((*top)->val <= (*top)->prev->val && (*top)->prev->val < val)
			|| ((*top)->prev->val >= (*top)->val && (*top)->val > val)));
}

int	is_val_placed_descending_order(int val, t_flap **top)
{
	if (!(*top))
		return (1);
	return ((((*top)->val < val && val < (*top)->prev->val)
			|| ((*top)->val >= (*top)->prev->val && (*top)->prev->val > val)
			|| ((*top)->prev->val <= (*top)->val && (*top)->val < val)));
}
