/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_sorted.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 15:47:10 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/08 16:03:33 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	is_sorted(t_flap *topa)
{
	t_flap	*save;

	save = topa;
	topa = topa->next;
	while (save != topa)
	{
		if (topa->prev->val > topa->val)
			return (0);
		topa = topa->next;
	}
	return (1);
}
