/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves_sides.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 17:05:01 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 12:05:34 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static const t_op	g_mv_fn[][2] = {
	{rr, ra},
	{rrr, rra},
	{rr, rb},
	{rrr, rrb},
	{ra, rrb},
	{rra, rb},
};

void	move_to_hitted_spot(int dir, int val, t_flap **topa, t_flap **topb)
{
	const t_op	*p_fun;

	p_fun = g_mv_fn[0];
	if (dir < 0)
	{
		p_fun = g_mv_fn[1];
		dir = -dir;
	}
	while (!is_val_placed_descending_order(val, topb))
	{
		p_fun[0](topa, topb);
		dir--;
	}
	while (dir)
	{
		p_fun[1](topa, topb);
		dir--;
	}
}

void	move_forward_further(int dir, int val, t_flap **topa, t_flap **topb)
{
	const t_op	*p_fun;

	p_fun = g_mv_fn[2];
	if (dir < 0)
	{
		p_fun = g_mv_fn[3];
		dir = -dir;
	}
	while (dir)
	{
		p_fun[0](topa, topb);
		dir--;
	}
	while (*topb && !is_val_placed_descending_order(val, topb))
		p_fun[1](topa, topb);
}

void	move_the_other_side(int dir, int val, t_flap **topa, t_flap **topb)
{
	const t_op	*p_fun;

	p_fun = g_mv_fn[4];
	if (dir < 0)
	{
		p_fun = g_mv_fn[5];
		dir = -dir;
	}
	while (dir)
	{
		p_fun[0](topa, topb);
		dir--;
	}
	while (*topb && !is_val_placed_descending_order(val, topb))
		p_fun[1](topa, topb);
}

/*
void	move_to_hitted_spot(int dir, int val, t_flap **topa,
				t_flap **topb, const t_op *p_fun)
{
	if (dir < 0)
	{
		p_fun++;
		dir = -dir;
	}
	if (!hitted_spot(dir, val, topb))
		while (dir)
		{
			p_fun[1](topa, topb);
			dir--;
		}
	while (!is_val_placed_descending_order(val, topb))
	{
		p_fun[0](topa, topb);
		dir--;
	}
	while (dir > 0)
	{
		p_fun[1](topa, topb);
		dir--;
	}
}
*/
