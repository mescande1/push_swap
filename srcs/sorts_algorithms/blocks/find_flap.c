/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_flap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 18:58:20 by matthieu          #+#    #+#             */
/*   Updated: 2021/09/27 12:03:49 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	get_val(t_flap *top, int dir)
{
	if (dir < 0)
		return (get_val(top->prev, dir + 1));
	if (dir > 0)
		return (get_val(top->next, dir - 1));
	return (top->val);
}

int	block_has_flap(t_flap *top, int i, int *bloc_size)
{
	int	k;
	int	len;

	len = spit_len(top);
	k = 0;
	while (k < len)
	{
		if (bloc_size[i - 1] <= top->val && top->val <= bloc_size[i])
			return (1);
		top = top->next;
		k++;
	}
	return (0);
}

int	hitted_spot(int dir, int val, t_flap *top)
{
	while (dir > 0)
	{
		if (is_val_placed_descending_order(val, &top))
			return (1);
		top = top->next;
		dir--;
	}
	while (dir < 0)
	{
		if (is_val_placed_descending_order(val, &top))
			return (1);
		top = top->prev;
		dir++;
	}
	return (0);
}

int	neg_is_shorter_way_forward(int dir, int val, t_flap *top)
{
	int		count;
	t_flap	*save;

	save = top;
	while (dir < 0)
	{
		top = top->prev;
		dir++;
	}
	while (!is_val_placed_descending_order(val, &top))
	{
		dir++;
		top = top->prev;
	}
	count = 0;
	while (count < dir && !is_val_placed_descending_order(val, &save))
	{
		count++;
		save = save->next;
	}
	if (dir - count > 0)
		return (0);
	return (1);
}

int	is_shorter_way_forward(int dir, int val, t_flap *top)
{
	int		count;
	t_flap	*save;

	if (dir < 0)
		return (neg_is_shorter_way_forward(dir, val, top));
	save = top;
	while (top && dir > 0)
	{
		top = top->next;
		dir--;
	}
	while (top && !is_val_placed_descending_order(val, &top))
	{
		dir++;
		top = top->next;
	}
	count = 0;
	while (top && count++ < dir + 1
		&& !is_val_placed_descending_order(val, &save))
		save = save->prev;
	if (dir - count > 0)
		return (0);
	return (1);
}
