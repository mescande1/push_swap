/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_sort_5.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 12:10:29 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/11 13:05:49 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	special_sort_3_extend(t_flap **topa, t_flap **topb)
{
	if ((*topa)->val < (*topa)->next->val
		&& (*topa)->next->val < (*topa)->prev->val)
		return ;
	if ((*topa)->val < (*topa)->prev->val
		&& (*topa)->prev->val < (*topa)->next->val)
		sa(topa, topb);
	if ((*topa)->prev->val < (*topa)->next->val
		&& (*topa)->next->val < (*topa)->val)
		ra(topa, topb);
	if ((*topa)->next->val < (*topa)->val && (*topa)->val < (*topa)->prev->val)
		sa(topa, topb);
	if ((*topa)->prev->val < (*topa)->val && (*topa)->val < (*topa)->next->val)
		rra(topa, topb);
	if ((*topa)->next->val < (*topa)->prev->val
		&& (*topa)->prev->val < (*topa)->val)
		ra(topa, topb);
}

void	special_sort_5(t_flap *topa, t_flap *topb)
{
	if (spit_len(topa) == 5)
		pb(&topa, &topb);
	pb(&topa, &topb);
	special_sort_3_extend(&topa, &topb);
	while (topb)
	{
		if (is_val_placed_ascending_order(topb->val, &topa))
			pa(&topa, &topb);
		else
			ra(&topa, &topb);
	}
	turn_sorted_ascending(&topa);
}
