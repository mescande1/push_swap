/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_val.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/19 12:45:06 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/08 15:44:27 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	out_of_range_verif(char *str, int pos, char sign)
{
	const int	intmax[] = {2, 1, 4, 7, 4, 8, 3, 6, 4, 7};

	if (str[0] - '0' > intmax[pos] + (pos != 0 && sign == '-' && pos % 9 == 0))
		return (error_print("Out of range value", 1));
	if (str[0] - '0' == intmax[pos] + (pos != 0 && sign == '-' && pos % 9 == 0))
		if (str[1])
			return (out_of_range_verif(str + 1, pos + 1, sign));
	return (0);
}

static int	verif_max(char *str)
{
	char	sign;

	sign = 0;
	if (str[0] == '-')
		sign = '-';
	return (out_of_range_verif(str + (sign == '-'), 0, sign));
}

int	verif_dup(int i, t_flap *top, t_flap *tab)
{
	if (tab == top)
		return (0);
	if (i == tab->true_val)
		return (error_print("Same number occurs twice", 1));
	return (verif_dup(i, top, tab->next));
}

int	verif_val(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!ft_isdigit(str[i]) && str[i] != '-')
			return (error_print("Non numeric value", 1));
	if (i > LEN_OF_INTMAX || (i == LEN_OF_INTMAX && verif_max(str)))
		return (error_print("Out of range value", 1));
	return (0);
}
