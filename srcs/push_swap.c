/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/12 11:26:50 by matthieu          #+#    #+#             */
/*   Updated: 2021/10/08 11:36:31 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	push_swap(int ac, char **av)
{
	t_flap	*topa;
	t_flap	*topb;

	topb = NULL;
	topa = create_spitflap(ac, av);
	if (!topa)
	{
		free(topa);
		return (1);
	}
	choose_sort(topa, topb, ac - 1);
	free(topa);
	return (0);
}
